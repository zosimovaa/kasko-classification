# Описание
Нужно построить модель отклика на предложение покупки полиса КАСКО – обучить логистическую регрессию, которая будет возвращать вероятность покупки.
Качество решения будет оцениваться по величине Gini (внимательнее к данным, имеет смысл их предварительно изучить) и общей аккуратности оформления. Модель должна быть валидирована и результаты валидации должны быть показаны в решении.

# Ссылки
Ноутбук с решением: [kasko.ipynb](https://bitbucket.org/zosimovaa/kasko-classification/src/master/kasko-2.0.ipynb?viewer=nbviewerw)



 